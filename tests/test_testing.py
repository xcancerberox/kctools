import unittest

import kctools.inverse_kinematics_solution as iks
from kctools.testing import CustomAssertions


class TestTestingCustomAssertionSolutions(unittest.TestCase, CustomAssertions):
    def test_assert_almost_equal_by_None(self):
        self.assertSolutionAlmostEqual(iks.IKSolution([None], [None]), iks.IKSolution([None], [None]))

    def test_assert_almost_equal_subsolution_by_None(self):
        self.assertSolutionAlmostEqual(iks.IKSolution([None], [iks.IKSolution([None], [None])]),
                                       iks.IKSolution([None], [iks.IKSolution([None], [None])]))

    def test_fail_assert_almost_equal_subsolution_by_not_None(self):
        with self.assertRaises(AssertionError):
            self.assertSolutionAlmostEqual(iks.IKSolution([0], [iks.IKSolution([None], [None])]),
                                           iks.IKSolution([None], [iks.IKSolution([None], [None])]))

    def test_assert_almost_equal(self):
        self.assertSolutionAlmostEqual(iks.IKSolution([3.1411], [iks.IKSolution([0], [180])]),
                                       iks.IKSolution([3.1412], [iks.IKSolution([0], [180])]), 3)

    def test_assert_almost_equal_arrownd_zero(self):
        self.assertSolutionAlmostEqual(iks.IKSolution([0.0], [0.0]),
                                       iks.IKSolution([-0.0], [-0.0]))

    def test_assert_almost_equal_int_float(self):
        self.assertSolutionAlmostEqual(iks.IKSolution([0.0], [0.0]),
                                       iks.IKSolution([0], [0]))

    def test_fail_assert_almost_equal_len_mismatch(self):
        with self.assertRaises(AssertionError):
            self.assertSolutionAlmostEqual(iks.IKSolution([0], [iks.IKSolution([0], [0])]),
                                           iks.IKSolution([iks.IKSolution([0], [0])], [0]))

    def test_fail_assert_almost_equal(self):
        with self.assertRaises(AssertionError):
            self.assertSolutionAlmostEqual(iks.IKSolution([3.1411], [iks.IKSolution([0], [180])]),
                                           iks.IKSolution([3.1412], [iks.IKSolution([0], [180])]), 4)


if __name__ == '__main__':
    unittest.main()
