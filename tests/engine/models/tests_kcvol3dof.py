import unittest
import numpy as np

import kctools.inverse_kinematics_solution as iks
from kctools.kcengine.models.kcvol3dof import KCVol3DOF
from kctools.testing import CustomAssertions


class KCVol3DOFTestEqualLen(unittest.TestCase, CustomAssertions):
    length = 10
    test_cases = {
            'case0': {
                'point': [length*3, 0, 0, 1],
                'solutions': iks.IKSolution([0, iks.IKSolution([0, 0], [0, 0])],
                                            [None])
                },
            'case1': {
                'point': [length*2, 0, length, 1],
                'solutions': iks.IKSolution([0, iks.IKSolution([-np.radians(90), np.radians(90)],
                                                               [0, -np.radians(90)])],
                                            [None])
                },
            'case2': {
                'point': [0, 0, -length, 1],
                'solutions': iks.IKSolution([0, iks.IKSolution([np.radians(90), np.radians(90)],
                                                               [np.radians(180), -np.radians(90)])],
                                            [np.pi, iks.IKSolution([np.radians(90), np.radians(90)],
                                                                   [np.radians(180), -np.radians(90)])])
                },
            'case3': {
                'point': [0, length*3, 0, 1],
                'solutions': iks.IKSolution([np.radians(90), iks.IKSolution([0, 0], [0, 0])],
                                            [None])
                },
            }

    def test_inverse_kinematics(self):
        kc_3dof = KCVol3DOF(self.length, self.length, self.length)
        for case, data in self.test_cases.items():
            with self.subTest(i=case):
                endpoint = np.array(data['point'])
                solutions = kc_3dof.inverse_kinematics(endpoint)
                self.assertSolutionAlmostEqual(solutions, data['solutions'])

    def Test_forward_kinematics(self):
        kc_3dof = KCVol3DOF(self.length, self.length, self.length)
        for case, data in self.test_cases.items():
            with self.subTest(i=case):
                for solution in ['positive', 'negative']:
                    solution_value = getattr(data['solutions'], solution)
                    if solution_value[0] is not None:
                        for solution_2dof in ['positive', 'negative']:
                            solution_2dof_value = getattr(solution_value[1], solution_2dof)
                            endpoint = kc_3dof.forward_kinematics(solution_value[0],
                                                                  solution_2dof_value[0],
                                                                  solution_2dof_value[1])
                            np.testing.assert_almost_equal(endpoint, np.array(data['point']))


class KCVol3DOFTestSubchain(unittest.TestCase):
    length = 10

    def setUp(self):
        self.kc_3dof = KCVol3DOF(self.length, self.length, self.length)

    def test_subchain_point_straigth_x(self):
        point = np.array([self.length*3, 0, 0, 1])
        relative_subchain_point = [self.length*2, 0, 0, 1]

        subchain_point = self.kc_3dof.calc_subchain_endpoint(0, point)
        np.testing.assert_array_almost_equal(subchain_point, relative_subchain_point)

    def test_subchain_point_straigth_xz(self):
        point = np.array([self.length*2, 0, self.length, 1])
        relative_subchain_point = [self.length, -self.length, 0, 1]

        subchain_point = self.kc_3dof.calc_subchain_endpoint(0, point)
        np.testing.assert_array_almost_equal(subchain_point, relative_subchain_point)

    def test_subchain_point_straigth_z(self):
        point = np.array([0, 0, -self.length, 1])
        relative_subchain_point = [-self.length, self.length, 0, 1]

        subchain_point = self.kc_3dof.calc_subchain_endpoint(0, point)
        np.testing.assert_array_almost_equal(subchain_point, relative_subchain_point)

    def test_subchain_point_straigth_y(self):
        point = np.array([0, self.length*3, 0, 1])
        relative_subchain_point = [self.length*2, 0, 0, 1]

        subchain_point = self.kc_3dof.calc_subchain_endpoint(np.radians(90), point)
        np.testing.assert_array_almost_equal(subchain_point, relative_subchain_point)


class KCVol3DOFTestReachable(unittest.TestCase):
    length = 10

    def test_reachable(self):
        kc_3dof = KCVol3DOF(self.length, self.length, self.length)
        self.assertTrue(kc_3dof.reachable(np.array([self.length*3, 0, 0, 1])))

    def test_unreachable_point_too_far(self):
        kc_3dof = KCVol3DOF(self.length, self.length, self.length)
        self.assertFalse(kc_3dof.reachable(np.array([self.length*3.1, 0, 0, 1])))

    def test_unreachable_point_too_close(self):
        kc_3dof = KCVol3DOF(3*self.length, self.length, self.length)
        self.assertFalse(kc_3dof.reachable(np.array([0.5*self.length, 0, 0, 1])))


if __name__ == '__main__':
    unittest.main()
