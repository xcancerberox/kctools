import unittest
import numpy as np

import kctools.inverse_kinematics_solution as iks
from kctools.kcengine.models.kcplane2dof import KCPlane2DOF
from kctools.testing import CustomAssertions


class KCPlane2DOFTest(unittest.TestCase, CustomAssertions):
    length = 4.5
    test_cases = {
            'case0': {
                'point': [length*2, 0, 0, 1],
                'solutions': iks.IKSolution([0, 0], [0, 0])
                },
            'case1': {
                'point': [length, -length, 0, 1],
                'solutions': iks.IKSolution([-np.radians(90), np.radians(90)], [0, -np.radians(90)])
                },
            'case2': {
                'point': [length, length, 0, 1],
                'solutions': iks.IKSolution([0, np.radians(90)], [np.radians(90), -np.radians(90)])
                },
            'case3': {
                'point': [-length, length, 0, 1],
                'solutions': iks.IKSolution([np.radians(90), np.radians(90)], [np.radians(180), -np.radians(90)])
                },
            'case4': {
                'point': [-length, -length, 0, 1],
                'solutions': iks.IKSolution([-np.radians(180), np.radians(90)], [-np.radians(90), -np.radians(90)])
                },
            'case5': {
                'point': [-length, length, 0, 1],
                'solutions': iks.IKSolution([np.radians(90), np.radians(90)], [np.radians(180), -np.radians(90)])
                },
            'unreachable': {
                'point': [3*length, 0, 0, 1],
                'solutions': iks.IKSolution([None], [None])
                },
            }

    def test_inverse_kinematics_equal_len(self):
        kc_2dof = KCPlane2DOF(self.length, self.length)
        for case, data in self.test_cases.items():
            with self.subTest(i=case):
                endpoint = np.array(data['point'])
                solutions = kc_2dof.inverse_kinematics(endpoint)
                self.assertSolutionAlmostEqual(solutions, data['solutions'])

    def test_forward_kinematics_equal_len(self):
        kc_2dof = KCPlane2DOF(self.length, self.length)
        for case, data in self.test_cases.items():
            with self.subTest(i=case):
                for solution in ['positive', 'negative']:
                    solution_value = getattr(data['solutions'], solution)
                    if solution_value[0] is not None:
                        endpoint = kc_2dof.forward_kinematics(solution_value[0], solution_value[1])
                        np.testing.assert_almost_equal(endpoint, np.array(data['point']))

    def test_reachable(self):
        kc_2dof = KCPlane2DOF(self.length, self.length)
        self.assertTrue(kc_2dof.reachable(np.array([self.length*2, 0, 0, 1])))

    def test_unreachable_point_out_of_plane(self):
        kc_2dof = KCPlane2DOF(self.length, self.length)
        self.assertFalse(kc_2dof.reachable(np.array([0, 0, 1, 1])))

    def test_unreachable_point_too_far(self):
        kc_2dof = KCPlane2DOF(self.length, self.length)
        self.assertFalse(kc_2dof.reachable(np.array([self.length*3, 0, 0, 1])))

    def test_unreachable_point_too_close(self):
        kc_2dof = KCPlane2DOF(self.length, 0.5*self.length)
        self.assertFalse(kc_2dof.reachable(np.array([0.3*self.length, 0, 0, 1])))


if __name__ == '__main__':
    unittest.main()
