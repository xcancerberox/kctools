import numpy as np
import unittest

from kctools.kcengine.transformations import rotation, translation


class TranslationTest(unittest.TestCase):
    def translation_test(self, translation_vector, initial, translated):
        transformation = translation(translation_vector)
        transformed = np.dot(transformation, initial.T).T
        np.testing.assert_array_equal(transformed, translated)

    def test_zero_only_x(self):
        self.translation_test(
            np.array([1, 0, 0, 1]),
            np.array([0, 0, 0, 1]),
            np.array([1, 0, 0, 1])
            )

    def test_zero_only_y(self):
        self.translation_test(
            np.array([0, 1, 0, 1]),
            np.array([0, 0, 0, 1]),
            np.array([0, 1, 0, 1])
            )

    def test_zero_only_z(self):
        self.translation_test(
            np.array([0, 0, 1, 1]),
            np.array([0, 0, 0, 1]),
            np.array([0, 0, 1, 1])
            )

    def test_zero_xyz(self):
        self.translation_test(
            np.array([1, 2, 3, 1]),
            np.array([0, 0, 0, 1]),
            np.array([1, 2, 3, 1])
            )

    def test_any_xyz(self):
        self.translation_test(
            np.array([1, 2, 3, 1]),
            np.array([-1, 20, -4, 1]),
            np.array([0, 22, -1, 1])
            )


class RotationTest(unittest.TestCase):
    def rotation_test(self, direction, angle, expected):
        transformation = rotation(direction, angle)
        np.testing.assert_array_almost_equal(transformation, expected)

    def test_no_angle(self):
        self.rotation_test(
                np.array([1, 0, 0]),
                0,
                np.array(
                    [
                        [1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_no_direction(self):
        self.rotation_test(
                np.array([0, 0, 0]),
                np.radians(90),
                np.array(
                    [
                        [1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_360(self):
        self.rotation_test(
                np.array([1, 2, 3]),
                np.radians(360),
                np.array(
                    [
                        [1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )
        
    def test_x_90(self):
        self.rotation_test(
                np.array([1, 0, 0]),
                np.radians(90),
                np.array(
                    [
                        [1, 0, 0, 0],
                        [0, 0, -1, 0],
                        [0, 1, 0, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_x_m90(self):
        self.rotation_test(
                np.array([1, 0, 0]),
                -np.radians(90),
                np.array(
                    [
                        [1, 0, 0, 0],
                        [0, 0, 1, 0],
                        [0, -1, 0, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_y_90(self):
        self.rotation_test(
                np.array([0, 1, 0]),
                np.radians(90),
                np.array(
                    [
                        [0, 0, 1, 0],
                        [0, 1, 0, 0],
                        [-1, 0, 0, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_y_m90(self):
        self.rotation_test(
                np.array([0, 1, 0]),
                -np.radians(90),
                np.array(
                    [
                        [0, 0, -1, 0],
                        [0, 1, 0, 0],
                        [1, 0, 0, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_z_90(self):
        self.rotation_test(
                np.array([0, 0, 1]),
                np.radians(90),
                np.array(
                    [
                        [0, -1, 0, 0],
                        [1, 0, 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_z_m90(self):
        self.rotation_test(
                np.array([0, 0, 1]),
                -np.radians(90),
                np.array(
                    [
                        [0, 1, 0, 0],
                        [-1, 0, 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )

    def test_mixed_rotation(self):
        self.rotation_test(
                np.array([1, 1, 1]),
                np.radians(120),
                np.array(
                    [
                        [0, 0, 1, 0],
                        [1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [0, 0, 0, 1],
                        ]
                    )
                )


if __name__ == '__main__':
    unittest.main()
