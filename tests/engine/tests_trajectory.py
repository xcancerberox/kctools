import unittest
import numpy as np

from kctools.kcengine import trajectory as ty


class TestTrajectoryOnJointSpaceCubicPolynomial(unittest.TestCase):
    def setUp(self):
        self.delta_t = 10
        self.angle_ini = -10
        self.angle_fin = 90
        self.trajectory = ty.JointSpaceTrajectoryCubicPolynomial(
                                                    self.angle_ini,
                                                    self.angle_fin,
                                                    self.delta_t)

    def test_initial_velocity_cero(self):
        np.testing.assert_almost_equal(self.trajectory.angle_velocity(0), 0)

    def test_final_velocity_cero(self):
        np.testing.assert_almost_equal(
                self.trajectory.angle_velocity(self.delta_t),
                0
                )

    def test_initial_point(self):
        np.testing.assert_almost_equal(
                self.trajectory.angle(0),
                self.angle_ini
                )

    def test_final_point(self):
        np.testing.assert_almost_equal(
                self.trajectory.angle(self.delta_t),
                self.angle_fin
                )

    def test_max_acceleration(self):
        self.assertRaises(
                ValueError,
                ty.JointSpaceTrajectoryCubicPolynomial,
                *(-10, 90, 1)
                )


class TestTrajectoryOnJointSpaceLinearParabolic(unittest.TestCase):
    def setUp(self):
        self.delta_t = 10
        self.angle_ini = -10
        self.angle_fin = 90
        self.trajectory = ty.JointSpaceTrajectoryLinearParabolic(
                                            [self.angle_ini, self.angle_fin],
                                            self.delta_t,
                                            30)

    def test_initial_velocity_cero(self):
        np.testing.assert_almost_equal(self.trajectory.angle_velocity(0), 0)

    def test_final_velocity_cero(self):
        np.testing.assert_almost_equal(
                self.trajectory.angle_velocity(self.delta_t),
                0
                )

    def test_initial_point(self):
        np.testing.assert_almost_equal(
                self.trajectory.angle(0),
                self.angle_ini
                )

    def test_final_point(self):
        np.testing.assert_almost_equal(
                self.trajectory.angle(self.delta_t),
                self.angle_fin
                )

    def test_max_acceleration(self):
        self.assertRaises(
                ValueError,
                ty.JointSpaceTrajectoryCubicPolynomial,
                *(-10, 90, 1)
                )

    def test_start_mix_point(self):
        e = 1e-10
        np.testing.assert_almost_equal(
                self.trajectory.angle(self.trajectory.t_mix - e),
                self.trajectory.angle(self.trajectory.t_mix + e)
                )

    def test_end_mix_point(self):
        e = 1e-10
        t_mix_end = self.trajectory.delta_t - self.trajectory.t_mix
        np.testing.assert_almost_equal(
                self.trajectory.angle(t_mix_end - e),
                self.trajectory.angle(t_mix_end + e)
                )

    def test_start_mix_point_velocity(self):
        e = 1e-10
        np.testing.assert_almost_equal(
                self.trajectory.angle_velocity(self.trajectory.t_mix - e),
                self.trajectory.angle_velocity(self.trajectory.t_mix + e)
                )

    def test_end_mix_point_velocity(self):
        e = 1e-10
        t_mix_end = self.trajectory.delta_t - self.trajectory.t_mix
        np.testing.assert_almost_equal(
                self.trajectory.angle_velocity(t_mix_end - e),
                self.trajectory.angle_velocity(t_mix_end + e)
                )


if __name__ == '__main__':
    unittest.main()
