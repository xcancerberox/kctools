import unittest

import kctools.inverse_kinematics_solution as iks


class TestInverseKinematicsSolution(unittest.TestCase):
    def test_not_list(self):
        with self.assertRaises(ValueError):
            iks.IKSolution(None, [None])

        with self.assertRaises(ValueError):
            iks.IKSolution([None], None)

    def test_equal_by_none(self):
        solution_one = iks.IKSolution([None], [None])
        solution_two = iks.IKSolution([None], [None])
        self.assertEqual(solution_one, solution_two)

    def test_not_equal_positive(self):
        solution_one = iks.IKSolution([0], [None])
        solution_two = iks.IKSolution([None], [None])
        self.assertNotEqual(solution_one, solution_two)

    def test_not_equal_negative(self):
        solution_one = iks.IKSolution([None], [0])
        solution_two = iks.IKSolution([None], [None])
        self.assertNotEqual(solution_one, solution_two)

    def test_equal_positive_sub_solution_by_None(self):
        solution_one = iks.IKSolution([iks.IKSolution([None], [None])], [None])
        solution_two = iks.IKSolution([iks.IKSolution([None], [None])], [None])
        self.assertEqual(solution_one, solution_two)

    def test_equal_negative_sub_solution_by_None(self):
        solution_one = iks.IKSolution([None], [iks.IKSolution([None], [None])])
        solution_two = iks.IKSolution([None], [iks.IKSolution([None], [None])])
        self.assertEqual(solution_one, solution_two)

    def test_equal(self):
        solution_one = iks.IKSolution([0], [iks.IKSolution([90], [180])])
        solution_two = iks.IKSolution([0], [iks.IKSolution([90], [180])])
        self.assertEqual(solution_one, solution_two)

    def test_not_equal(self):
        solution_one = iks.IKSolution([0], [iks.IKSolution([91], [180])])
        solution_two = iks.IKSolution([0], [iks.IKSolution([90], [180])])
        self.assertNotEqual(solution_one, solution_two)

    def test_length(self):
        self.assertEqual(3, len(iks.IKSolution([0, iks.IKSolution([0, 1], [None])], [None])))


if __name__ == '__main__':
    unittest.main()
