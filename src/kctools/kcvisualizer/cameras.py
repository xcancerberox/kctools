import numpy as np
import pyglet.gl as gl
from pyglet.window import key

from kctools.mylogging import logger


class Camera():
    def __init__(self, window_size):
        self.window_size = window_size

    def update(self):
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.gluPerspective(35, self.window_size[0]/self.window_size[1], 1, 10000)
        gl.glMatrixMode(gl.GL_MODELVIEW)


class FollowingCamera(Camera):
    def __init__(self, window_size, radius, rotation, elevation, center=[0, 0, 0]):
        super().__init__(window_size)

        self.center = center
        self.radius = radius
        self.alfa = rotation
        self.gamma = elevation

    def rotate_elevation(self, angle):
        self.gamma += angle

    def rotate_pitch(self, angle):
        self.alfa += angle

    def zoom(self, delta_radius):
        self.radius += delta_radius

    def update(self):
        super().update()

        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()
        up_vector = [
            -np.cos(np.radians(90-self.gamma))*np.cos(np.radians(self.alfa)),
            -np.cos(np.radians(90-self.gamma))*np.sin(np.radians(self.alfa)),
            np.sin(np.radians(90-self.gamma))
            ]

        eye = [
            self.center[0] + self.radius*np.cos(np.radians(self.gamma))*np.cos(np.radians(self.alfa)),
            self.center[1] + self.radius*np.cos(np.radians(self.gamma))*np.sin(np.radians(self.alfa)),
            self.center[2] + self.radius*np.sin(np.radians(self.gamma))
            ]
        gl.gluLookAt(*(eye + self.center + up_vector))
        logger.info("Following camera update. {} ({}, {})".format(self.radius,
                                                                  self.alfa,
                                                                  self.gamma))
        logger.info("eye: {}, up: {}".format(eye, up_vector))


class FreeCamera(Camera):
    def __init__(self, window_size, rotation, elevation, eye=[500, 500, 500]):
        super().__init__(window_size)

        self.eye = eye
        self.alfa = rotation
        self.gamma = elevation
        self.radius = 100
        self.update_up_vector()
        self.update_center()

    def rotate_elevation(self, angle):
        self.gamma += angle

    def rotate_pitch(self, angle):
        self.alfa += angle

    def update_up_vector(self):
        self.up_vector = [
            -np.cos(np.radians(90-self.gamma))*np.cos(np.radians(self.alfa)),
            -np.cos(np.radians(90-self.gamma))*np.sin(np.radians(self.alfa)),
            np.sin(np.radians(90-self.gamma))
            ]

    def update_center(self):
        self.center = [
            self.eye[0] + self.radius*np.cos(np.radians(self.gamma))*np.cos(np.radians(self.alfa)),
            self.eye[1] + self.radius*np.cos(np.radians(self.gamma))*np.sin(np.radians(self.alfa)),
            self.eye[2] + self.radius*np.sin(np.radians(self.gamma))
            ]

    def update(self):
        super().update()

        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()
        self.update_up_vector()
        self.update_center()

        gl.gluLookAt(*(self.eye + self.center + self.up_vector))
        logger.info("Free camera update. {} ({}, {})".format(self.radius,
                                                             self.alfa,
                                                             self.gamma))
        logger.info("eye: {}, center: {}, up: {}".format(self.eye, self.center, self.up_vector))


class CameraControls():
    def __init__(self, visualizer):
        self.visualizer = visualizer
        self.interface = None

        self.mouse_callbacks = {}
        self.keyboard_callbacks = {}
        self.on_hold_callbacks = {}


class FollowingCameraControls(CameraControls):
    def __init__(self, cam, visualizer):
        super().__init__(visualizer)

        self.cam = cam

        self.keyboard_callbacks[key.A] = {'on_key_press': self.rotate_pitch_ccw}
        self.keyboard_callbacks[key.S] = {'on_key_press': self.rotate_elevation_ccw}
        self.keyboard_callbacks[key.D] = {'on_key_press': self.rotate_pitch_cw}
        self.keyboard_callbacks[key.W] = {'on_key_press': self.rotate_elevation_cw}
        self.mouse_callbacks['scroll'] = self.zoom

    def rotate_elevation_ccw(self, modifier):
        self.cam.rotate_elevation(5)

    def rotate_elevation_cw(self, modifier):
        self.cam.rotate_elevation(-5)

    def rotate_pitch_ccw(self, modifier):
        self.cam.rotate_pitch(5)

    def rotate_pitch_cw(self, modifier):
        self.cam.rotate_pitch(-5)

    def zoom(self, x, y, scroll_x, scroll_y):
        self.cam.zoom(scroll_y*10)


class FreeCameraControls(CameraControls):
    def __init__(self, cam, visualizer, sensibility=0.5):
        super().__init__(visualizer)

        self.cam = cam

        if sensibility > 1 or sensibility < 0.1:
            raise ValueError('The sensibility is not posible (0.1 <= sensibility <= 1)')

        self.sensibility = sensibility
        self.exclusive = False

        self.keyboard_callbacks[key.S] = {'on_key_press': self.move_eye_backwards}
        self.on_hold_callbacks[key.S] = self.move_eye_backwards
        self.keyboard_callbacks[key.W] = {'on_hold_press': self.move_eye_forward}
        self.on_hold_callbacks[key.W] = self.move_eye_forward
        self.keyboard_callbacks[key.H] = {'on_key_press': self.exclusive_mouse_toggle}
        self.mouse_callbacks['on_mouse_motion'] = self.change_center

    def get_center_eye_direction(self):
        center = np.array(self.cam.center)
        eye = np.array(self.cam.eye)
        direction = center - eye
        direction = direction/np.linalg.norm(direction)

        return center, eye, direction

    def exclusive_mouse_toggle(self, modifier):
        self.exclusive ^= True
        self.visualizer.window.set_exclusive_mouse(self.exclusive)

    def move_eye_backwards(self, modifier):
        center, eye, direction = self.get_center_eye_direction()
        self.cam.eye = list(eye - direction * 10)

    def move_eye_forward(self, modifier):
        center, eye, direction = self.get_center_eye_direction()
        self.cam.eye = list(eye + direction * 10)

    def change_center(self, x, y, dx, dy):
        if self.exclusive:
            self.cam.rotate_elevation(dy * self.sensibility)
            self.cam.rotate_pitch(-dx * self.sensibility)
