import pyglet.gl as gl
import numpy as np


def vec(*args):
    return (gl.GLfloat * len(args))(*args)


def inspect_gl():
    a = (gl.GLfloat * 16)()
    b = (gl.GLfloat * 16)()
    gl.glGetFloatv(gl.GL_MODELVIEW_MATRIX, a)
    gl.glGetFloatv(gl.GL_PROJECTION_MATRIX, b)
    print(np.reshape(np.array([i for i in a]), (4, 4)))
    print(np.reshape(np.array([i for i in b]), (4, 4)))
