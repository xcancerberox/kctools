import kctools.kcvisualizer.models.base as base_models


class KCVol3DOFVisualizerModel(base_models.DrawingBlock):
    def __init__(self, position, rotation, angles, lengths, axis_visible=True):
        super().__init__(position, rotation)

        self.joint_1 = base_models.Joint1DOF_Z(angles[0])
        self.joint_2 = base_models.Joint1DOF_Y(angles[1])
        self.joint_3 = base_models.Joint1DOF_Y(angles[2])

        link_1 = base_models.Link(lengths[0])
        link_2 = base_models.Link(lengths[1])
        link_3 = base_models.Link(lengths[2]*0.8)
        chain_end = base_models.LinkEnd(link_3.radius, lengths[2]*0.2)

        if axis_visible:
            axis = base_models.Axis(width=1.2, length=30)
            self.add_child(axis)

        self.add_child(self.joint_1)
        self.joint_1.add_child(link_1)
        link_1.add_child(self.joint_2)
        self.joint_2.add_child(link_2)
        link_2.add_child(self.joint_3)
        self.joint_3.add_child(link_3)
        link_3.add_child(chain_end)

    def set_angles(self, angles):
        self.joint_1.set_angle(angles[0])
        self.joint_2.set_angle(angles[1])
        self.joint_3.set_angle(angles[2])
