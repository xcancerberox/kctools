import kctools.kcvisualizer.models.base as base_models
from kctools.kcvisualizer.models.kcvol3dof import KCVol3DOFVisualizerModel


class Hexapod6DOFVisualizerModel(base_models.DrawingBlock):
    def __init__(self, position, rotation, morphology, axis_visible=True):
        super().__init__(position, rotation)
        leg_lengths = [morphology.legs_lengths]*6
        legs_position = []
        for leg in morphology.legs_config:
            legs_position.append(leg['position'])
        body = base_models.Body(legs_position, morphology.body_config['body_height'])
        head = base_models.Cuboid(
                                size=(10, 10, 10),
                                position=(0, morphology.body_config['body_length']/2, 0, 1),
                                )
        axis = base_models.Axis(width=0.5, color=(1, 1, 0))

        self.legs = []
        for leg_number in range(6):
            self.legs.append(
                    KCVol3DOFVisualizerModel(
                        position=morphology.legs_config[leg_number]['position'],
                        rotation=morphology.legs_config[leg_number]['rotation'],
                        angles=morphology.legs_initial_angles[leg_number],
                        lengths=leg_lengths[leg_number],
                        axis_visible=axis_visible
                        )
                    )

        self.add_child(axis)
        self.add_child(body)
        self.add_child(head)
        for leg in self.legs:
            self.add_child(leg)

    def set_angles(self, angles):
        for index, leg in enumerate(self.legs):
            leg.set_angles(angles[index])
