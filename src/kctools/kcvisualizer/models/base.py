import pyglet
import pyglet.gl as gl

import numpy as np


class Node():
    """
    Logic connection between parts for permanent rotation and translation.
    """
    def __init__(self, position, rotation):
        self.position = position
        self.rotation = rotation

    def draw(self):
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glTranslatef(*self.position)
        gl.glRotatef(*self.rotation)


class Drawing():
    def __init__(self, position, rotation):
        self.position = position
        self.rotation = rotation

    def design(self):
        pass

    def draw(self):
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glPushMatrix()
        gl.glTranslatef(*self.position)
        gl.glRotatef(*self.rotation)

        self.design()

        gl.glPopMatrix()


class Arrow(Drawing):
    def __init__(self, position=[0]*3, rotation=[0]*4, width=0.8, length=100, colorf=(1, 1, 1)):
        super().__init__(position, rotation)
        self.width = width
        self.length = length
        self.colorf = colorf
        self.quad = gl.gluNewQuadric()

    def design(self):
        gl.glColor3f(*self.colorf)
        gl.gluCylinder(self.quad, self.width, self.width, self.length, 60, 20)
        gl.glTranslatef(0, 0, self.length)
        gl.gluCylinder(self.quad, self.width*1.5, 0, self.width*3, 60, 20)


class Cuboid(Drawing):
    def __init__(self, size, position=[0]*3, rotation=[0]*4, color=(1, 0, 0)):
        super().__init__(position, rotation)
        self.center = [0]*3
        self.size = size
        self.batch = pyglet.graphics.Batch()

        self.batch.add(
                24,
                gl.GL_QUADS,
                None,
                ('v3f', self.cuboid_vertices()),
                ('c3f', (color*24))
                )

    def cuboid_vertices(self):
        x = self.center[0] - self.size[0]/2
        y = self.center[1] - self.size[1]/2
        z = self.center[2] - self.size[2]/2
        X = x + self.size[0]
        Y = y + self.size[1]
        Z = z + self.size[2]

        return (x, y, z, X, y, z, X, Y, z, x, Y, z,
                x, y, Z, X, y, Z, X, Y, Z, x, Y, Z,
                x, Y, z, X, Y, z, X, Y, Z, x, Y, Z,
                x, y, z, X, y, z, X, y, Z, x, y, Z,
                x, y, z, x, Y, z, x, Y, Z, x, y, Z,
                X, y, z, X, Y, z, X, Y, Z, X, y, Z)

    def design(self):
        self.batch.draw()


class Body(Drawing):
    def __init__(self, points, height, position=[0]*3, rotation=[0]*4):
        super().__init__(position, rotation)
        self.batch = pyglet.graphics.Batch()
        self.lines = pyglet.graphics.Batch()

        center_up = [0, 0, height/2]
        center_down = [0, 0, -height/2]

        for i in range(len(points)):
            for j in range(len(points)):
                self.batch.add(
                        3,
                        gl.GL_TRIANGLES,
                        None,
                        ('v3f', (
                            points[i][0], points[i][1], points[i][2],
                            points[j][0], points[j][1], points[j][2],
                            center_up[0], center_up[1], center_up[2],
                            )),
                        ('c3f', ((1, 0, 0)*3))
                        )
                self.batch.add(
                        3,
                        gl.GL_TRIANGLES,
                        None,
                        ('v3f', (
                            points[i][0], points[i][1], points[i][2],
                            points[j][0], points[j][1], points[j][2],
                            center_down[0], center_down[1], center_down[2],
                            )),
                        ('c3f', ((1, 0, 0)*3))
                        )

    def design(self):
        self.batch.draw()
        self.lines.draw()


class Cylinder(Drawing):
    def __init__(self, radius, height, color, position, rotation):
        self.radius = radius
        self.height = height
        self.color = color
        self.quad = gl.gluNewQuadric()
        super().__init__(position, rotation)

    def design(self):
        gl.glColor3f(*self.color)
        gl.gluCylinder(self.quad, self.radius, self.radius,
                       self.height, 60, 20)


class Cone(Drawing):
    def __init__(self, radius, height, color, position, rotation):
        self.radius = radius
        self.height = height
        self.color = color
        self.quad = gl.gluNewQuadric()
        super().__init__(position, rotation)

    def design(self):
        gl.glColor3f(*self.color)
        gl.gluCylinder(self.quad, self.radius, 0,
                       self.height, 60, 20)


class Disk(Drawing):
    def __init__(self, radius, color, position, rotation):
        self.radius = radius
        self.color = color
        self.quad = gl.gluNewQuadric()
        super().__init__(position, rotation)

    def design(self):
        gl.glColor3f(*self.color)
        gl.gluDisk(self.quad, 0, self.radius, 60, 20)


class DrawingBlock(Drawing):
    def __init__(self, position, rotation):
        self.children = []
        super().__init__(position, rotation)

    def add_child(self, child):
        self.children.append(child)

    def design(self):
        for child in self.children:
            child.draw()


class Axis(DrawingBlock):
    def __init__(self, position=[0]*3, rotation=[0]*4, width=0.4, length=40, color=(1, 0, 0)):
        super().__init__(position, rotation)
        self.width = width
        self.length = length

        self.add_child(Arrow([0]*3, [0, 0, 0, 0], width, length, color))
        self.add_child(Arrow([0]*3, [90, 0, 1, 0], width, length, color))
        self.add_child(Arrow([0]*3, [-90, 1, 0, 0], width, length, color))


class Floor(Drawing):
    def __init__(self):
        super().__init__([0]*3, [0]*4)
        self.batch = pyglet.graphics.Batch()

        tile_size = 50
        tile_count = 100

        for i in range(tile_count):
            for j in range(tile_count):
                if (i % 2) ^ (j % 2) == 1:
                    color = (0.8, 0.8, 1, 0.8,)
                else:
                    color = (0.005, 0.005, 0.005, 0.8,)
                x = i * tile_size - (tile_count * tile_size)/2
                y = j * tile_size - (tile_count * tile_size)/2
                X = (i+1) * tile_size - (tile_count * tile_size)/2
                Y = (j+1) * tile_size - (tile_count * tile_size)/2

                self.batch.add(
                        4,
                        gl.GL_QUADS,
                        None,
                        ('v3f', tuple([x, y, 0, X, y, 0, X, Y, 0, x, Y, 0])),
                        ('c4f', color*4)
                        )

    def design(self):
        self.batch.draw()


class Link(DrawingBlock):
    def __init__(self, length, position=[0]*3, rotation=[0]*4):
        super().__init__(position, rotation)
        self.length = length
        self.radius = length*0.05

        self.add_child(Cylinder(self.radius, length, [1, 0, 0],
                                [0]*3, [90, 0, 1, 0]))
        self.add_child(Node([length, 0, 0], [0]*4))


class LinkEnd(DrawingBlock):
    def __init__(self, radius, length, position=[0]*3, rotation=[0]*4):
        super().__init__(position, rotation)
        self.length = length

        self.add_child(Cone(radius, length, [1, 0, 0], [0]*3, [90, 0, 1, 0]))
        self.add_child(Node([length, 0, 0], [0]*4))


class Joint1DOF(DrawingBlock):
    def __init__(self, angle, position=[0]*3, rotation=[0]*4):
        super().__init__(position, [0]*4)
        self.angle = angle
        self.beta = np.radians(rotation[0])
        self.node = Node([0]*3, [self.angle, 0,
                                 np.cos(self.beta), np.sin(self.beta)])

        length = 30
        radius = 10

        joint_drawing = DrawingBlock([0]*3, rotation)
        joint_drawing.add_child(Cylinder(radius, length,
                                         color=[0, 0, 1],
                                         position=[0, length/2, 0],
                                         rotation=[90, 1, 0, 0]))
        joint_drawing.add_child(Disk(radius,
                                     color=[0, 0, 1],
                                     position=[0, -length/2, 0],
                                     rotation=[90, 1, 0, 0]))
        joint_drawing.add_child(Disk(radius,
                                     color=[0, 0, 1],
                                     position=[0, length/2, 0],
                                     rotation=[90, 1, 0, 0]))

        self.add_child(joint_drawing)
        self.add_child(self.node)

    def set_angle(self, angle):
        self.angle = angle
        self.node.rotation = [self.angle, 0, np.cos(self.beta),
                              np.sin(self.beta)]


class Joint1DOF_X(Joint1DOF):
    def __init__(self, angle):
        super().__init__(angle, rotation=[90, 0, 0, 1])


class Joint1DOF_Y(Joint1DOF):
    def __init__(self, angle):
        super().__init__(angle)


class Joint1DOF_Z(Joint1DOF):
    def __init__(self, angle):
        super().__init__(angle, rotation=[90, 1, 0, 0])


class Link_X(Link):
    def __init__(self, length):
        super().__init__(length)


class Link_Y(Link):
    def __init__(self, length):
        super().__init__(length, rotation=[90, 0, 0, 1])


class Link_Z(Link):
    def __init__(self, length):
        super().__init__(length, rotation=[-90, 0, 1, 0])
