﻿import pyglet
from pyglet.window import key, mouse
import pyglet.gl as gl

from kctools.mylogging import logger
from kctools.kcvisualizer.tools import vec

import kctools.kcvisualizer.cameras as cameras


WINDOW_SIZE = (1280, 720)


def callback_count_check(name, count):
    if count == 1:
        check_ok = True
    elif count == 0:
        logger.info('No callback configured for {}'.format(name))
        check_ok = False
    else:
        logger.info('Too many callbacks configured for {}'.format(name))
        check_ok = False
    return check_ok


class MainWindow(pyglet.window.Window):
    def __init__(self, cameras, active_camera, scene):
        super().__init__(*WINDOW_SIZE, resizable=True)
        self.scene = scene
        self.cameras = cameras
        self.active_camera = active_camera

        self.keyboard_callbacks = []
        self.on_hold_callbacks = []
        self.mouse_callbacks = []

        self.hold_keys = key.KeyStateHandler()
        self.push_handlers(self.hold_keys)
        pyglet.clock.schedule(self.key_hold_handler, 1/10)

        gl.glClearColor(0, 0, 0, 1)
        gl.glEnable(gl.GL_DEPTH_TEST)

    def on_resize(self, width, height):
        gl.glViewport(0, 0, width, height)
        for camera in self.cameras:
            camera.window_size = (width, height)
        self.cameras[self.active_camera].update()

    def on_draw(self):
        self.clear()
        self.cameras[self.active_camera].update()
        self.scene.draw()

    def on_key_press(self, symbol, modifier):
        callback_count = 0
        for keyboard_callback in self.keyboard_callbacks:
            if symbol in keyboard_callback:
                if 'on_key_press' in keyboard_callback[symbol].keys():
                    keyboard_callback[symbol]['on_key_press'](modifier)
                    callback_count += 1
        callback_count_check('on_key_press {}'.format(symbol), callback_count)

    def on_key_release(self, symbol, modifier):
        callback_count = 0
        for keyboard_callback in self.keyboard_callbacks:
            if symbol in keyboard_callback:
                if 'on_key_release' in keyboard_callback[symbol].keys():
                    keyboard_callback[symbol]['on_key_release'](modifier)
                    callback_count += 1
        callback_count_check('on_key_release {}'.format(symbol), callback_count)

    def key_hold_handler(self, dt, f):
        for on_hold_callback in self.on_hold_callbacks:
            for symbol, callback in on_hold_callback.items():
                if symbol in self.hold_keys.keys():
                    if self.hold_keys[symbol]:
                        callback(None)

    def on_mouse_press(self, x, y, button, modifiers):
        pass

    def on_mouse_release(self, x, y, button, modifiers):
        pass

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        callback_count = 0
        for mouse_callback in self.mouse_callbacks:
            if buttons == mouse.LEFT:
                mouse_callback['left_drag'](x, y, dx, dy)
                callback_count += 1
            elif buttons == mouse.RIGHT:
                mouse_callback['right_drag'](x, y, dx, dy)
                callback_count += 1
        callback_count_check('on_mouse_drag', callback_count)

    def on_mouse_motion(self, x, y, dx, dy):
        callback_count = 0
        for mouse_callback in self.mouse_callbacks:
            if 'on_mouse_motion' in mouse_callback.keys():
                mouse_callback['on_mouse_motion'](x, y, dx, dy)
                callback_count += 1
        callback_count_check('on_mouse_motion', callback_count)

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        callback_count = 0
        for mouse_callback in self.mouse_callbacks:
            if 'scroll' in mouse_callback.keys():
                mouse_callback['scroll'](x, y, scroll_x, scroll_y)
                callback_count += 1
        callback_count_check('on_mouse_scroll', callback_count)


class Scene():
    def __init__(self, models):
        self.models = models

    def lights(self):
        gl.glEnable(gl.GL_LIGHTING)
        gl.glEnable(gl.GL_LIGHT0)
        gl.glColorMaterial(gl.GL_FRONT_AND_BACK, gl.GL_EMISSION)
        gl.glEnable(gl.GL_COLOR_MATERIAL)
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_AMBIENT, vec(1, 1, 1, 1.0))
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPECULAR, vec(1, 1, 1, 1))
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_DIFFUSE, vec(0.6, 0.6, 0.6, 1))
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, vec(0, -100, 100, 1))
        gl.glEnable(gl.GL_NORMALIZE)
        gl.glShadeModel(gl.GL_SMOOTH)

    def draw(self):
        self.lights()

        for model in self.models.values():
            model.draw()


class KCVisualizer(pyglet.app.EventLoop):
    def __init__(self, models):
        super().__init__()

        self.models = models
        self.scene = Scene(self.models)

        self.cameras = [cameras.FreeCamera(WINDOW_SIZE, 225, -30),
                        cameras.FollowingCamera(WINDOW_SIZE, 300, 0, 0)]
        self.window = MainWindow(self.cameras, 0, self.scene)

        self.keyboard_callbacks = {}
        self.keyboard_callbacks[key.ESCAPE] = {'on_key_press': self.exit}
        self.keyboard_callbacks[key._1] = {'on_key_press': self.set_free_camera}
        self.keyboard_callbacks[key._2] = {'on_key_press': self.set_following_camera}

        self.window.keyboard_callbacks.append(self.keyboard_callbacks)

        self.camera_control = [cameras.FreeCameraControls(self.cameras[0], self),
                               cameras.FollowingCameraControls(self.cameras[1], self)]
        self.set_active_camera(0)

    def set_active_camera(self, active_camera):
        prev_active_camera = self.window.active_camera
        try:
            index = self.window.keyboard_callbacks.index(self.camera_control[prev_active_camera].keyboard_callbacks)
            self.window.keyboard_callbacks.pop(index)
        except ValueError:
            logger.info('Callbacks not in keyboard_callbacks')

        try:
            index = self.window.on_hold_callbacks.index(self.camera_control[prev_active_camera].on_hold_callbacks)
            self.window.on_hold_callbacks.pop(index)
        except ValueError:
            logger.info('Callbacks not in on_hold_callbacks')

        try:
            index = self.window.mouse_callbacks.index(self.camera_control[prev_active_camera].mouse_callbacks)
            self.window.mouse_callbacks.pop(index)
        except ValueError:
            logger.info('Callbacks not in mouse_callbacks')

        self.window.active_camera = active_camera
        self.camera = self.cameras[active_camera]
        self.window.keyboard_callbacks.append(self.camera_control[active_camera].keyboard_callbacks)
        self.window.on_hold_callbacks.append(self.camera_control[active_camera].on_hold_callbacks)
        self.window.mouse_callbacks.append(self.camera_control[active_camera].mouse_callbacks)

    def exit(self, modifier):
        super().exit()

    def set_free_camera(self, modifier):
        self.set_active_camera(0)

    def set_following_camera(self, modifier):
        self.set_active_camera(1)
