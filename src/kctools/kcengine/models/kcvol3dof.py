import numpy as np

from kctools.kcengine.transformations import translation, rotation
from kctools.kcengine.models.kcplane2dof import KCPlane2DOF
from kctools.mylogging import logger
import kctools.inverse_kinematics_solution as iks



class KCVol3DOF():
    """
    Kinematic chain with 3 rotational joints and 3 links
    Joint1DOF_Z(theta1)
        --> Link_X(l1)
            --> Fixed_rotation_X(-90)
                --> Joint1DOF_Z(theta2)
                    --> Link_X(l2)
                        --> Joint1DOF_Z(theta3)
                            --> Link_X(l3)
    """
    def __init__(self, l1, l2, l3):
        self.l1 = l1
        self.l2 = l2
        self.l3 = l3

        self.sub_kc = KCPlane2DOF(l2, l3)

    def calc_subchain_endpoint(self, theta, point):
        rotation_x_90 = rotation(np.array([1, 0, 0]), np.radians(90))
        translation_l1 = translation(np.array([-self.l1, 0, 0, 1]))
        rotation_z_theta = rotation(np.array([0, 0, 1]), -theta)

        rototated_point = np.dot(rotation_z_theta, point.T)
        rotation_x90_point = np.dot(rotation_x_90, rototated_point)
        rototranslated_point = np.dot(translation_l1, rotation_x90_point)

        return rototranslated_point.T

    def forward_kinematics(self, theta1, theta2, theta3):
        logger.info('KCVol3DOF forward kinematics for angles {}, {}, {}'.format(theta1, theta2, theta3))
        pose = np.array(
                [
                    [
                        np.cos(theta1)*np.cos(theta2+theta3),
                        -np.cos(theta1)*np.sin(theta2+theta3),
                        -np.sin(theta1),
                        np.cos(theta1)*(self.l1+self.l2*np.cos(theta2)+self.l3*np.cos(theta2+theta3))
                        ],
                    [
                        np.sin(theta1)*np.cos(theta2+theta3),
                        -np.sin(theta1)*np.sin(theta2+theta3),
                        np.cos(theta1),
                        np.sin(theta1)*(self.l1+self.l2*np.cos(theta2)+self.l3*np.cos(theta2+theta3))
                        ],
                    [
                        -np.sin(theta2+theta3),
                        -np.cos(theta2+theta3),
                        0,
                        -(self.l2*np.sin(theta2)+self.l3*np.sin(theta2+theta3))
                        ],
                    [
                        0,
                        0,
                        0,
                        1,
                        ],
                    ]
                )

        return pose[0:4, 3].reshape(4,)

    def inverse_kinematics(self, endpoint):
        logger.info('KCVol3DOF inverse kinematics for point {}'.format(endpoint))

        if self.reachable(endpoint):
            solutions = self.theta1_solutions(endpoint)

            for solution in ['positive', 'negative']:
                subchain_endpoint = self.calc_subchain_endpoint(getattr(solutions, solution)[0], endpoint)
                if self.sub_kc.reachable(subchain_endpoint):
                    solutions_2dof = self.sub_kc.inverse_kinematics(subchain_endpoint)
                    getattr(solutions, solution).append(solutions_2dof)
                else:
                    setattr(solutions, solution, [None])
        else:
            solutions = iks.IKSolution([None], [None])

        return solutions

    def theta1_solutions(self, endpoint):
        theta1 = np.arctan2(endpoint[1], endpoint[0])
        return iks.IKSolution([theta1], [theta1 + np.pi])

    def reachable(self, endpoint):
        max_reach = self.l1 + self.l2 + self.l3
        point_module = np.sqrt(endpoint[0]**2 +
                               endpoint[1]**2 +
                               endpoint[2]**2)

        if point_module > max_reach:
            logger.info('Point not reachable by module {} > max_reach {}'.format(point_module, max_reach))
            return False
        logger.info('Point module is reachable {}'.format(point_module))

        reachable = False

        solutions = self.theta1_solutions(endpoint)
        for solution in ['positive', 'negative']:
            subchain_endpoint = self.calc_subchain_endpoint(getattr(solutions, solution)[0], endpoint)
            reachable |= self.sub_kc.reachable(subchain_endpoint)

        return reachable
