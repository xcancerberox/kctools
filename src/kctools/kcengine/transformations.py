import numpy as np


def translation(translation_vector):
    if type(translation_vector) is not np.ndarray:
        raise TypeError
    if not translation_vector.shape == (4,):
        raise ValueError

    transformation = np.identity(4)
    transformation[0:4, 3] = translation_vector.T
    return transformation


def rotation(direction, angle):
    norm = np.linalg.norm(direction)

    if norm == 0:
        rotation = np.identity(4)
    else:
        wx = direction[0]/norm
        wy = direction[1]/norm
        wz = direction[2]/norm

        rotation = np.array([
            [
                np.cos(angle) + wx**2*(1-np.cos(angle)),
                wx*wy*(1-np.cos(angle)) - wz*np.sin(angle),
                wx*wz*(1-np.cos(angle)) + wy*np.sin(angle),
                0
                ],
            [
                wy*wx*(1-np.cos(angle)) + wz*np.sin(angle),
                np.cos(angle) + wy**2*(1-np.cos(angle)),
                wy*wz*(1-np.cos(angle)) - wx*np.sin(angle),
                0
                ],
            [
                wz*wx*(1-np.cos(angle)) - wy*np.sin(angle),
                wz*wy*(1-np.cos(angle)) + wx*np.sin(angle),
                np.cos(angle) + wz**2*(1-np.cos(angle)),
                0
                ],
            [
                0,
                0,
                0,
                1
                ],
            ])

    return rotation
