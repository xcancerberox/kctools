import numpy as np
import numpy.polynomial.polynomial as poly


class JointSpaceTrajectoryCubicPolynomial():
    def __init__(self, angle_ini, angle_fin, delta_t, angle_acc_max=40):
        if angle_acc_max < 6*(angle_fin - angle_ini)/delta_t**2:
            raise ValueError('Acceleration needed out of boundaries')

        a0 = angle_ini
        a1 = 0
        a2 = (angle_fin - angle_ini) * (3/delta_t**2)
        a3 = (angle_fin - angle_ini) * (-2/delta_t**3)

        self.angle_coef = np.array([a0, a1, a2, a3])
        self.angle_velocity_coef = poly.polyder(self.angle_coef)
        self.angle_acceleration_coef = poly.polyder(self.angle_velocity_coef)

    def angle(self, time):
        return poly.polyval(time, self.angle_coef)

    def angle_velocity(self, time):
        return poly.polyval(time, self.angle_velocity_coef)

    def angle_acceleration(self, time):
        return poly.polyval(time, self.angle_acceleration_coef)


class JointSpaceTrajectoryLinearParabolic():
    def __init__(self, angles, delta_t, acc, angle_acc_max=40):
        self.angle_ini = angles[0]
        self.angle_fin = angles[1]
        self.delta_t = delta_t
        self.angle_acc_max = angle_acc_max
        self.angle_acc_min = 4*(self.angle_fin - self.angle_ini)/delta_t**2
        self.acc = acc

        if angle_acc_max < self.acc:
            raise ValueError('Acceleration needed out of boundaries')

        quadratic_on_tmix_coef = [self.acc,
                                  -self.acc * self.delta_t,
                                  (self.angle_fin - self.angle_ini)]
        # Negative solution
        self.t_mix = np.roots(quadratic_on_tmix_coef)[1]

        self.start_coef = np.array([
                                self.angle_ini,
                                0,
                                0.5*self.acc
                                ])
        self.start_velocity_coef = poly.polyder(self.start_coef)
        self.start_acceleration_coef = poly.polyder(self.start_velocity_coef)

        angle_mix = poly.polyval(self.t_mix, self.start_coef)

        angle_center = (self.angle_fin + self.angle_ini)/2
        t_center = delta_t/2

        line_slope = (angle_center - angle_mix)/(t_center - self.t_mix)
        self.linear_coef = np.array([
                                angle_mix - self.t_mix*line_slope,
                                line_slope
                                ])
        self.linear_velocity_coef = poly.polyder(self.linear_coef)
        self.linear_acceleration_coef = poly.polyder(self.linear_velocity_coef)

        self.end_coef = np.array([
                                self.angle_fin,
                                0,
                                -0.5*self.acc
                                ])
        self.end_velocity_coef = poly.polyder(self.end_coef)
        self.end_acceleration_coef = poly.polyder(self.end_velocity_coef)

    def conditions(self, time):
        return [
                time < self.t_mix,
                (time < (self.delta_t - self.t_mix)) * (time >= self.t_mix),
                time >= (self.delta_t - self.t_mix)
                ]

    def angle(self, time):
        functions = [
                lambda time: poly.polyval(
                                time,
                                self.start_coef
                                ),
                lambda time: poly.polyval(
                                time,
                                self.linear_coef
                                ),
                lambda time: poly.polyval(
                                time - self.delta_t,
                                self.end_coef
                                ),
                ]

        return np.piecewise(time, self.conditions(time), functions)

    def angle_velocity(self, time):
        functions = [
                lambda time: poly.polyval(
                                time,
                                self.start_velocity_coef
                                ),
                lambda time: poly.polyval(
                                time,
                                self.linear_velocity_coef
                                ),
                lambda time: poly.polyval(
                                time - self.delta_t,
                                self.end_velocity_coef
                                ),
                ]

        return np.piecewise(time, self.conditions(time), functions)

    def angle_acceleration(self, time):
        functions = [
                lambda time: poly.polyval(
                                time,
                                self.start_acceleration_coef
                                ),
                lambda time: poly.polyval(
                                time,
                                self.linear_acceleration_coef
                                ),
                lambda time: poly.polyval(
                                time - self.delta_t,
                                self.end_acceleration_coef
                                ),
                ]

        return np.piecewise(time, self.conditions(time), functions)
