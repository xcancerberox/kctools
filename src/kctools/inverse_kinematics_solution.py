

class IKSolution:
    def __init__(self, positive, negative):
        if not isinstance(positive, list) or not isinstance(negative, list):
            raise ValueError

        self.positive = positive
        self.negative = negative

    def __len__(self):
        length_positive = 0
        for solution in self.positive:
            if isinstance(solution, IKSolution):
                length_positive += len(solution)
            else:
                length_positive += 1

        length_negative = 0
        for solution in self.negative:
            if isinstance(solution, IKSolution):
                length_negative += len(solution)
            else:
                length_negative += 1

        return max(length_positive, length_negative)

    def __eq__(self, other):
        equal = True

        if isinstance(self.positive, type(other.positive)):
            equal &= (self.positive == other.positive)
        else:
            return False

        if isinstance(self.negative, type(other.negative)):
            equal &= (self.negative == other.negative)
        else:
            return False

        return equal

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        repre = "Positive: ["
        for solution in self.positive:
            repre += "{}, ".format(solution)

        repre += "]; Negative: ["
        for solution in self.negative:
            repre += "{}, ".format(solution)
        repre += "]"

        return repre
