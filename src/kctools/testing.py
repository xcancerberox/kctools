import kctools.inverse_kinematics_solution as iks


class CustomAssertions:
    def _assertSubSolutionAlmostEqual(self, solution_one, solution_two, depth):
        almos_eq = True

        for index, solution in enumerate(solution_one):
            if solution is None:
                if solution_two[index] is None:
                    almos_eq &= True
                else:
                    almos_eq &= False
            elif isinstance(solution, iks.IKSolution):
                if isinstance(solution_two[index], iks.IKSolution):
                    self.assertSolutionAlmostEqual(solution, solution_two[index])
                else:
                    almos_eq &= False

            elif isinstance(solution, int) or isinstance(solution, float):
                if isinstance(solution_two[index], int) or isinstance(solution_two[index], float):
                    almos_eq &= (round(solution - solution_two[index], depth) == 0)
                else:
                    almos_eq &= False
            else:
                almos_eq &= False
        return almos_eq

    def assertSolutionAlmostEqual(self, solution_one, solution_two, depth=7):
        almos_eq = True

        if len(solution_one) != len(solution_two):
            raise AssertionError("Solutions not almost equal, len mismatch: {} != {}".format(solution_one,
                                                                                             solution_two))

        almos_eq &= self._assertSubSolutionAlmostEqual(solution_one.positive, solution_two.positive, depth)
        almos_eq &= self._assertSubSolutionAlmostEqual(solution_one.negative, solution_two.negative, depth)

        if not almos_eq:
            raise AssertionError("Solutions not almost equal: {} != {}".format(solution_one,
                                                                               solution_two))
