from kcvisualizer import KCVisualizer
from kcvisualizer.models.tools import model_from_description
from kcvisualizer.models.kcvol3dof import KCVol3DOFVisualizerModel
import kcvisualizer.models.base as base_models


if __name__ == '__main__':
    arm_3dof = KCVol3DOFVisualizerModel(
                                position=[0, 0, 0, 1],
                                rotation=[0]*4,
                                angles=[0, 0, 0],
                                lengths=[50, 50, 30])
    world_axis = base_models.Axis()

    models = {
            'arm': arm_3dof,
            'world_axis': world_axis
            }
    visualizer = KCVisualizer(models)
    visualizer.models['arm'].set_angles([45, 45, 45])
    visualizer.run()
