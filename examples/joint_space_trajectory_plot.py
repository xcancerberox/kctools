import matplotlib.pyplot as plt
import numpy as np
import kcengine.trajectory as trajectory


def curve_on_subplot(title, data, subplot):
    subplot.set_title(title)

    subplot.set_xlabel(data['xlabel'])
    subplot.set_ylabel(data['ylabel'])

    subplot.grid(which='both')

    subplot.plot(data['xdata'], data['ydata'])


ta = 10
example_trajectory = trajectory.JointSpaceTrajectoryCubicPolynomial(-10, 90, ta)

time = np.arange(0, ta, 0.1)

fig = plt.figure()
angle_plot = fig.add_subplot(3, 1, 1)
curve_on_subplot('Trajectory between two points',
                 {
                     'ylabel': 'Angle (deg)',
                     'ydata': example_trajectory.angle(time),
                     'xlabel': 'time (s)',
                     'xdata': time,
                     },
                 angle_plot)
angle_plot = fig.add_subplot(3, 1, 2)
curve_on_subplot('Velocity',
                 {
                     'ylabel': 'Velocity (deg/s)',
                     'ydata': example_trajectory.angle_velocity(time),
                     'xlabel': 'time (s)',
                     'xdata': time,
                     },
                 angle_plot)
angle_plot = fig.add_subplot(3, 1, 3)
curve_on_subplot('Acceleration',
                 {
                     'ylabel': 'Acc (deg/s^2)',
                     'ydata': example_trajectory.angle_acceleration(time),
                     'xlabel': 'time (s)',
                     'xdata': time,
                     },
                 angle_plot)

plt.show()
