from kcvisualizer import KCVisualizer, models
from kcvisualizer.models.tools import model_from_description


arm_3dof_description = {
        'type': 'DrawingBlock',
        'parameters': {'position': [0]*3, 'rotation': [0]*4},
        'children': [
            {
                'type': 'Joint1DOF_Z',
                'parameters': {'angle': 45},
                'children': [
                    {
                        'type': 'Link',
                        'parameters': {'length': 10},
                        'children': [
                            {
                                'type': 'Joint1DOF_Y',
                                'parameters': {'angle': 45},
                                'children': [
                                    {
                                        'type': 'Link',
                                        'parameters': {'length': 50},
                                        'children': [
                                            {
                                                'type': 'Joint1DOF_Y',
                                                'parameters': {'angle': 45},
                                                'children': [
                                                    {
                                                        'type': 'Link',
                                                        'parameters': {'length': 30},
                                                        'children': []
                                                    }]
                                            }]
                                    }]
                            }]
                    }]
            }]
        }

if __name__ == '__main__':
    arm_3dof = model_from_description(arm_3dof_description)
    world_axis = models.base.Axis()

    models = {
            'arm': arm_3dof,
            'world_axis': world_axis
            }
    visualizer = KCVisualizer(models)

    visualizer.run()
