import matplotlib.pyplot as plt
import numpy as np
import kcengine.trajectory as trajectory


fig = plt.figure()
angle_plot = fig.add_subplot(3, 1, 1)
velocity_plot = fig.add_subplot(3, 1, 2)
acc_plot = fig.add_subplot(3, 1, 3)


def plot_trajectory(angle_plot, velocity_plot, acc_plot,
                    angle_ini, angle_fin, acc):
    ta = 10
    new_trajectory = trajectory.JointSpaceTrajectoryLinearParabolic(
                                            [angle_ini, angle_fin],
                                            ta,
                                            acc,
                                            angle_acc_max=100)
    time = np.arange(0, ta, 0.01)
    t_mix_end = new_trajectory.delta_t - new_trajectory.t_mix

    angle_plot.set_title('Trajectory between two points')
    angle_plot.set_xlabel('Angle (deg)')
    angle_plot.set_ylabel('time (s)')

    angle_plot.grid(which='both')
    angle_plot.axvline(new_trajectory.t_mix, color='r', linestyle='--')
    angle_plot.axvline(t_mix_end, color='r', linestyle='--')

    angle_plot.plot(time, new_trajectory.angle(time))

    velocity_plot.set_xlabel('Velocity (deg)')
    velocity_plot.set_ylabel('time (s)')

    velocity_plot.grid(which='both')
    velocity_plot.axvline(new_trajectory.t_mix, color='r', linestyle='--')
    velocity_plot.axvline(t_mix_end, color='r', linestyle='--')

    velocity_plot.plot(time, new_trajectory.angle_velocity(time))

    acc_plot.set_xlabel('Acc (deg)')
    acc_plot.set_ylabel('time (s)')

    acc_plot.grid(which='both')
    acc_plot.axvline(new_trajectory.t_mix, color='r', linestyle='--')
    acc_plot.axvline(t_mix_end, color='r', linestyle='--')
    acc_plot.axhline(new_trajectory.angle_acc_max, color='r', linestyle='--')
    acc_plot.axhline(new_trajectory.angle_acc_min, color='r', linestyle='--')
    acc_plot.axhline(-new_trajectory.angle_acc_max, color='r', linestyle='--')
    acc_plot.axhline(-new_trajectory.angle_acc_min, color='r', linestyle='--')

    acc_plot.plot(time, new_trajectory.angle_acceleration(time))


plot_trajectory(angle_plot, velocity_plot, acc_plot, -90, 90, 7.5)
plot_trajectory(angle_plot, velocity_plot, acc_plot, -90, 90, 10)
plot_trajectory(angle_plot, velocity_plot, acc_plot, -90, 90, 20)
plot_trajectory(angle_plot, velocity_plot, acc_plot, -90, 90, 30)
plot_trajectory(angle_plot, velocity_plot, acc_plot, -90, 90, 40)

plt.show()
