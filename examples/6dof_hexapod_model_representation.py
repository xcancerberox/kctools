from kcvisualizer import KCVisualizer, models
from kcvisualizer.models.tools import model_from_description
from kcvisualizer.models.kchexapod6dof import Hexapod6DOFVisualizerModel


if __name__ == '__main__':
    hexapod = Hexapod6DOFVisualizerModel(
                        position=[0, 0, 50, 1],
                        rotation=[0, 0, 0, 0],
                        body_length=150,
                        body_width=80,
                        leg_lengths=[[10, 50, 30]]*6,
                        )
    world_axis = models.base.Axis(length=30)

    models = {
            'hexapod': hexapod,
            'world_axis': world_axis
            }
    visualizer = KCVisualizer(models)
    visualizer.models['hexapod'].set_angles([
                        [30, 45, 45],
                        [0, 45, 45],
                        [-30, 45, 45],
                        [-30, 45, 45],
                        [0, 45, 45],
                        [30, 45, 45],
                        ])

    visualizer.run()
