from kcvisualizer import KCVisualizer
import kcvisualizer.models.base as base_models


if __name__ == '__main__':
    world_axis = base_models.Axis()
    world_floor = base_models.Floor()

    models = {
            'world_axis': world_axis,
            'world_floor': world_floor,
            }

    visualizer = KCVisualizer(models)
    visualizer.run()
