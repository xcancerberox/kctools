from kcvisualizer import KCVisualizer, models
from kcvisualizer.models.tools import model_from_description


arm_3dof_children = [
            {
                'type': 'Joint1DOF_Z',
                'parameters': {'angle': 0},
                'children': [
                    {
                        'type': 'Link',
                        'parameters': {'length': 10},
                        'children': [
                            {
                                'type': 'Joint1DOF_Y',
                                'parameters': {'angle': 45},
                                'children': [
                                    {
                                        'type': 'Link',
                                        'parameters': {'length': 50},
                                        'children': [
                                            {
                                                'type': 'Joint1DOF_Y',
                                                'parameters': {'angle': 45},
                                                'children': [
                                                    {
                                                        'type': 'Link',
                                                        'parameters': {'length': 30},
                                                        'children': []
                                                    }]
                                            }]
                                    }]
                            }]
                    }]
            }]

blen = 150
bwidth = 80
hexapod_6dof_description = {
        'type': 'DrawingBlock',
        'parameters': {'position': [0]*3, 'rotation': [0]*4},
        'children': [
                    {
                        'type': 'Cuboid',
                        'parameters': {
                            'position': [0]*3,
                            'rotation': [0]*4,
                            'size': (bwidth, blen, 10)
                            },
                        'children': []
                    },
                    {
                        'type': 'DrawingBlock',
                        'parameters': {'position': [bwidth/2, blen/2, 0, 1], 'rotation': [0]*4},
                        'children': arm_3dof_children
                    },
                    {
                        'type': 'DrawingBlock',
                        'parameters': {'position': [bwidth/2, 0, 0, 1], 'rotation': [0]*4},
                        'children': arm_3dof_children
                    },
                    {
                        'type': 'DrawingBlock',
                        'parameters': {'position': [bwidth/2, -blen/2, 0, 1], 'rotation': [0]*4},
                        'children': arm_3dof_children
                    },
                    {
                        'type': 'DrawingBlock',
                        'parameters': {'position': [-bwidth/2, blen/2, 0, 1], 'rotation': [180, 0, 0, 1]},
                        'children': arm_3dof_children
                    },
                    {
                        'type': 'DrawingBlock',
                        'parameters': {'position': [-bwidth/2, 0, 0, 1], 'rotation': [180, 0, 0, 1]},
                        'children': arm_3dof_children
                    },
                    {
                        'type': 'DrawingBlock',
                        'parameters': {'position': [-bwidth/2, -blen/2, 0, 1], 'rotation': [180, 0, 0, 1]},
                        'children': arm_3dof_children
                    },
        ]}


if __name__ == '__main__':
    hexapod = model_from_description(hexapod_6dof_description)
    world_axis = models.base.Axis()

    models = {
            'hexapod': hexapod,
            'world_axis': world_axis
            }
    visualizer = KCVisualizer(models)

    visualizer.run()
