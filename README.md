# KCTools

Kinematic chains tools

This code is intended to be a library for:
* Visualizer: kinematic chains representation in a 3d environment.
* Engine: kinematic chains inverse and forward kinematics computation.